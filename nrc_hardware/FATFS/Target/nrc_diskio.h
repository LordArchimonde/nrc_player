#ifndef NRC_DISKIO
#define NRC_DISKIO

#include "diskio.h"

inline DSTATUS NRC_disk_initialize (BYTE);
inline DSTATUS NRC_disk_status (BYTE);
inline DRESULT NRC_disk_read (BYTE, BYTE*, DWORD, BYTE);
inline DRESULT NRC_disk_write (BYTE, const BYTE*, DWORD, BYTE);
inline DRESULT NRC_disk_ioctl (BYTE, BYTE, void*);

/* Martin Thomas begin */

/* Card type flags (CardType) */
#define CT_MMC				0x01
#define CT_SD1				0x02
#define CT_SD2				0x04
#define CT_SDC				(CT_SD1|CT_SD2)
#define CT_BLOCK			0x08

#ifndef RAMFUNC
#define RAMFUNC
#endif
RAMFUNC void	disk_timerproc (void);

/* Martin Thomas end */


#endif // NRC_DISKIO
