/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "fatfs.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "ws2812b.h"
#include "ws2812b_conf.h"
#include "task.h"
#include "string.h"
#include "nrc_diskio.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
DMA_HandleTypeDef hdma_tim4_ch1;

UART_HandleTypeDef huart1;

osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */

#define NUM_LEDS    9

HSV_t leds[NUM_LEDS];

volatile uint8_t Timer1 = 0, Timer2 = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM5_Init(void);
static void MX_USART1_UART_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
void rgbTask(void *pvParameters)
{
	static uint16_t hue = 0;
	while (1) {
		while (!ws2812b_IsReady()); // wait

		leds[0].h = 180;
		leds[0].s = leds[0].v = 255;

		for (int i = 1; i < NUM_LEDS; i++) {
			leds[i].h = hue + i * 15;
			leds[i].s = 255;
			leds[i].v = 255;
		}

		ws2812b_SendHSV(leds, NUM_LEDS);
		hue += 1; if (hue >= 360) { hue = 0; }

		vTaskDelay(20);
	}
}


FATFS fs;  // file system
FIL fil;  // file
char buffer[1024]; // to store data

/* to send the data to the uart */
void send_uart(char* string)
{
	uint8_t len = strlen(string);
	HAL_UART_Transmit(&huart1, (uint8_t*)string, len, 2000);  // transmit in blocking mode
}

/* to find the size of data in the buffer */
int bufsize(char* buf)
{
	int i = 0;
	while (*buf++ != '\0') i++;
	return i;
}

void bufclear(void)  // clear buffer
{
	for (int i = 0; i < 1024; i++) {
		buffer[i] = '\0';
	}
}

void microsdTest()
{
	/* Mount SD Card */

	uint16_t br, bw;   // file read/write count

	/* capacity related variables */
	FATFS* pfs;
	DWORD fre_clust;
	uint32_t total, free_space;

	FRESULT fresult = f_mount(&fs, "", 1);
	if (fresult != FR_OK) {
		send_uart("error in mounting SD CARD...\n");
		return;
	}
	else {
		send_uart("SD CARD mounted successfully...\n");
	}


	/*************** Card capacity details ********************/

		/* Check free space */
	f_getfree("", &fre_clust, &pfs);

	total = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
	sprintf(buffer, "SD CARD Total Size: \t%lu\n", total);
	send_uart(buffer);
	bufclear();
	free_space = (uint32_t)(fre_clust * pfs->csize * 0.5);
	sprintf(buffer, "SD CARD Free Space: \t%lu\n", free_space);
	send_uart(buffer);


	/************* The following operation is using PUTS and GETS *********************/


	/* Open file to write/ create a file if it doesn't exist */
	fresult = f_open(&fil, "file1.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);

	/* Writing text */
	fresult = f_puts("This data is from the First FILE\n\n", &fil);

	/* Close file */
	fresult = f_close(&fil);

	send_uart("File1.txt created and the data is written \n");

	/* Open file to read */
	fresult = f_open(&fil, "file1.txt", FA_READ);

	/* Read string from the file */
	f_gets(buffer, fil.fsize, &fil);

	send_uart(buffer);

	/* Close file */
	f_close(&fil);

	bufclear();


	/**************** The following operation is using f_write and f_read **************************/

	/* Create second file with read write access and open it */
	fresult = f_open(&fil, "file2.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);

	/* Writing text */
	strcpy(buffer, "This is File 2 and it says Hello from controllerstech\n");

	fresult = f_write(&fil, buffer, bufsize(buffer), &bw);

	send_uart("File2.txt created and data is written\n");

	/* Close file */
	f_close(&fil);



	// clearing buffer to show that result obtained is from the file
	bufclear();

	/* Open second file to read */
	fresult = f_open(&fil, "file2.txt", FA_READ);

	/* Read data from the file
	 * Please see the function details for the arguments */
	f_read(&fil, buffer, fil.fsize, &br);
	send_uart(buffer);

	/* Close file */
	f_close(&fil);

	bufclear();


	/*********************UPDATING an existing file ***************************/

	/* Open the file with write access */
	fresult = f_open(&fil, "file2.txt", FA_OPEN_ALWAYS | FA_WRITE);

	/* Move to offset to the end of the file */
	fresult = f_lseek(&fil, fil.fsize);

	/* write the string to the file */
	fresult = f_puts("This is updated data and it should be in the end \n", &fil);

	f_close(&fil);

	/* Open to read the file */
	fresult = f_open(&fil, "file2.txt", FA_READ);

	/* Read string from the file */
	f_read(&fil, buffer, fil.fsize, &br);
	send_uart(buffer);

	/* Close file */
	f_close(&fil);

	bufclear();


	/*************************REMOVING FILES FROM THE DIRECTORY ****************************/

	fresult = f_unlink("/file1.txt");
	if (fresult == FR_OK) send_uart("file1.txt removed successfully...\n");

	fresult = f_unlink("/file2.txt");
	if (fresult == FR_OK) send_uart("file2.txt removed successfully...\n");

	/* Unmount SDCARD */
	fresult = f_mount(NULL, "", 1);
	if (fresult == FR_OK) send_uart("SD CARD UNMOUNTED successfully...\n");
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//#define MIN(a,b) ({ typeof(a) _a = (a); typeof(b) _b = (b); _a > _b ? _a : _b; })
//#define MAX(a,b) ({ typeof(a) _a = (a); typeof(b) _b = (b); _a < _b ? _a : _b; })
static inline int MAX(int a, int b) { return a > b ? a : b; }
static inline uint16_t MIN(uint16_t a, uint16_t b) { return a < b ? a : b; }
enum { NO_EFFECT = 0, MAYATNIK, NAKLADIVANIE };
uint8_t show_mode = MAYATNIK;
uint8_t neon_bright = 8;
char update_data[6] = { 0, 0, 0, 0, 0, 0 };
const uint8_t cathode_tab[6][10] = {
	{0,1,2,3,4,5,6,7,8,9},
	{0,1,2,3,4,5,6,7,8,9},
	{0,1,2,3,4,5,6,7,8,9},
	{0,1,2,3,4,5,6,7,8,9},
	{0,1,2,3,4,5,6,7,8,9},
	{0,1,2,3,4,5,6,7,8,9}
};
const uint8_t anode_tab[6] = { 5, 4, 3, 2, 1, 0 };
uint8_t speed = 2;

// пины C0-C5 предназначены для анодов
static inline void reset_anodes()
{
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5, GPIO_PIN_RESET);
}

// пины A8, A11, A12, B7, B8, B9, B11, C10, C11, C12 - катоды
void set_digit(uint8_t digit, uint8_t num)
{
	reset_anodes();
	if (digit < 10) {
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8 | GPIO_PIN_11 | GPIO_PIN_12, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_11, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12, GPIO_PIN_RESET);

#define NRC_SET_CATHODE(port,pin) HAL_GPIO_WritePin(GPIO##port, GPIO_PIN_##pin, GPIO_PIN_SET); break

		// катод 
		uint8_t tmp = cathode_tab[num][digit];
		switch(tmp) {
		case 0: NRC_SET_CATHODE(A, 8);
		case 1: NRC_SET_CATHODE(A, 11);
		case 2: NRC_SET_CATHODE(A, 12);
		case 3: NRC_SET_CATHODE(B, 7);
		case 4: NRC_SET_CATHODE(B, 8);
		case 5: NRC_SET_CATHODE(B, 9);
		case 6: NRC_SET_CATHODE(B, 11);
		case 7: NRC_SET_CATHODE(C, 10);
		case 8: NRC_SET_CATHODE(C, 11);
		case 9: NRC_SET_CATHODE(C, 12);
		}

		// анод
		tmp = anode_tab[num];
		HAL_GPIO_WritePin(GPIOC, 0x1 << (tmp), GPIO_PIN_SET);
	}
}

void nixieHandler(TIM_HandleTypeDef *htim)
{
	static uint8_t number = 0;
	//static uint8_t cnt = 0;
	static char data[6] = { 0, 0, 0, 0, 0, 0 };
	static char data2[6] = { 0, 0, 0, 0, 0, 0 };
	static int8_t bright[6] = { 0, 0, 0, 0, 0, 0 };
	static int8_t bright2[6] = { 0, 0, 0, 0, 0, 0 };
	uint16_t arr = __HAL_TIM_GET_AUTORELOAD(htim);

	switch(htim->Channel) {
	case HAL_TIM_ACTIVE_CHANNEL_CLEARED: {
		if (number < 5)number++;
		else number = 0;

		__HAL_TIM_DISABLE_IT(htim, TIM_IT_CC1);
		__HAL_TIM_DISABLE_IT(htim, TIM_IT_CC2);

		switch(show_mode) {
		case MAYATNIK: {
			if (update_data[number] == data[number]) {
				bright[number] = MIN(bright[number] + speed, 99);
			}
			else if (bright[number] > 0) {
				bright[number] = MAX(bright[number] - speed, 0);
			}
			else {
				data[number] = update_data[number];
			}

			if (bright[number]) {
				set_digit(data[number], number);
				__HAL_TIM_ENABLE_IT(htim, TIM_IT_CC1);
				__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_1, MIN(bright[number] * neon_bright, arr - 1));
			}
			else reset_anodes();
			break;
		}
		case NAKLADIVANIE: {
			if (update_data[number] == data[number]) {
				bright[number] = MIN(bright[number] + speed, 99);
				bright2[number] = MAX(bright2[number] - speed, 0);
			}
			else if (update_data[number] == data2[number]) {
				bright[number] = MAX(bright[number] - speed, 0);
				bright2[number] = MIN(bright2[number] + speed, 99);
			}
			else if (bright[number] == 0) {
				data[number] = update_data[number];
				bright2[number] = MAX(bright2[number] - speed, 0);
			}
			else if (bright2[number] == 0) {
				data2[number] = update_data[number];
				bright[number] = MAX(bright[number] - speed, 0);
			}
			else {
				bright[number] = MAX(bright[number] - speed, 0);
				bright2[number] = MAX(bright2[number] - speed, 0);
			}

			if (bright[number]) {
				set_digit(data[number], number);
				__HAL_TIM_ENABLE_IT(htim, TIM_IT_CC1);
				uint16_t ccr1 = MIN(bright[number] * neon_bright, arr - 1);
				__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_1, ccr1);
				if (bright2[number]) {
					__HAL_TIM_ENABLE_IT(htim, TIM_IT_CC2);
					__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_2, MIN(ccr1 + bright2[number] * neon_bright, arr - 1));
				}
			}
			else if (bright2[number]) {
				set_digit(data2[number], number);
				__HAL_TIM_ENABLE_IT(htim, TIM_IT_CC1);
				__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_1, MIN(bright2[number] * neon_bright, arr - 1));
			}
			else {
				reset_anodes();
			}
			break;
		}
		case NO_EFFECT: {
			set_digit(update_data[number], number);
			__HAL_TIM_ENABLE_IT(htim, TIM_IT_CC1);
			__HAL_TIM_SET_COMPARE(htim, TIM_CHANNEL_1, MIN(99 * neon_bright, arr - 1));
			break;
		}
		}

		// Несмотря на то что к этому моменту флаг TIM_SR_UIF уже сброшен вышестоящей функцией,
		// вручную обнуляем счетчик и сбрасываем сразу все флаги прерываний этого таймера,
		// чтобы дальше все было с чистого листа.
		__HAL_TIM_SET_COUNTER(htim, 0);
		__HAL_TIM_CLEAR_FLAG(htim, TIM_FLAG_UPDATE | TIM_FLAG_CC1 | TIM_FLAG_CC2);
		break;
	}
	case HAL_TIM_ACTIVE_CHANNEL_1: {
		if (show_mode == NAKLADIVANIE) {
			if (bright[number] == 0) { reset_anodes(); }
			else if (bright2[number]) { set_digit(data2[number], number); }
			else { reset_anodes(); }
		}
		else { reset_anodes(); }
		break;
	}
	case HAL_TIM_ACTIVE_CHANNEL_2: {
		reset_anodes();
		break;
	}
	default: break;
	}
}

// вызывается когда срабатывает прерывание канала захвата/сравнения таймера
// флаги прерываний обнуляются в вышестоящих функциях
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
	nixieHandler(htim);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  ws2812b_Init(&hdma_tim4_ch1, &htim4);
  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 750);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  xTaskCreate(rgbTask, "rgbTask", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
  HAL_TIM_Base_Start_IT(&htim5);
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.Prediv1Source = RCC_PREDIV1_SOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  RCC_OscInitStruct.PLL2.PLL2State = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the Systick interrupt time 
  */
  __HAL_RCC_PLLI2S_ENABLE();
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 89;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 119;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 1000;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_OC_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PD2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* init code for FATFS */
  MX_FATFS_Init();
  /* USER CODE BEGIN 5 */
  microsdTest();
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET);
	  vTaskDelay(1000);
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET);
	  vTaskDelay(1000);
    osDelay(1);
  }
  /* USER CODE END 5 */ 
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

	if (htim->Instance == TIM6) {
		disk_timerproc();
	}
  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  else if (htim->Instance == TIM5) {
	nixieHandler(htim);
  }

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
