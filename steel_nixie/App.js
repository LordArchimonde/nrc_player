/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import WifiManager from 'react-native-wifi-reborn';
import {PermissionsAndroid} from 'react-native';

import { LoginButton } from 'react-native-fbsdk'
import { GoogleSignin, GoogleSigninButton } from '@react-native-community/google-signin';
 
//export async function SNC_requestPermission(permission, title) 
async function SNC_requestPermission(permission, title) 
{
  try {
    // проверка что разрешения уже предоставлены
    let granted = true;
    granted = await PermissionsAndroid.check(permission);
    if (granted) {
      return true;
    }

    // непосредственно запрос разрешения
    granted = await PermissionsAndroid.request(
      permission,
      {
        'title': 'steel_nixie',
        'message': 'steel_nixie request access to ' + title
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the " + title)
      alert("You can use the " + title);
      return true;
    }
    else {
      console.log(title + " permission denied")
      alert(title + " permission denied");
      return false;
    }
  } catch (err) {
    console.warn(err);
    return false;
  }
}

function SNC_WifiItem({ id, wifiData, selected, onSelect }) {
  return (
    <TouchableOpacity
      onPress={() => onSelect(id)}
      style={[
        styles.item,
        { backgroundColor: selected ? '#6e3b6e' : '#f9c2ff' },
      ]}
    >
      <Text style={styles.title}>{wifiData.SSID}</Text>
      { !!selected &&
        Object.keys(wifiData).map(
          (objKey, idx) => { return (objKey !== wifiData.SSID) &&
            <Text key={idx}>-->{objKey}: {wifiData[objKey]}</Text>;
          }
        )
      }
    </TouchableOpacity>
  );
}

function SNC_WifiList() {
  const [selected, setSelected] = React.useState(new Map());
  const [wifiList, setWifiList] = React.useState([]);
  const onSelect = React.useCallback(
    id => {
      const newSelected = new Map(selected);
      newSelected.set(id, !selected.get(id));
      setSelected(newSelected);
    },
    [selected],
  );

  React.useEffect(() => {
    WifiManager.loadWifiList(
      (_wifiList) => {
        setWifiList(JSON.parse(_wifiList));
      },
      (error) => {
        console.log("SNC: loadWifiList err: " + error);
      }
    );
  }, [])
  return (
    <FlatList
      data={wifiList}
      renderItem={({ item }) => (
        <SNC_WifiItem
          id={item.SSID}
          wifiData={item}
          selected={!!selected.get(item.SSID)}
          onSelect={onSelect}
        />
      )}
      keyExtractor={item => item.SSID}
      extraData={selected}
    />
  );
}

function App () {
  [wifiPermissionsGranted, setGranted] = React.useState(false);
  React.useEffect(() => {
    const checkAllPermissions = async () => {
      const allPermissions = [
        { type: "android.permission.ACCESS_COARSE_LOCATION", title: "location" },
        { type: "android.permission.INTERNET", title: "internet" },
        { type: "android.permission.CHANGE_WIFI_STATE", title: "change wifi" },
        { type: "android.permission.ACCESS_WIFI_STATE", title: "access wifi" },
        { type: "android.permission.CHANGE_NETWORK_STATE", title: "change network" },
        { type: "android.permission.ACCESS_NETWORK_STATE", title: "access network" },
      ];
      let granted = false;
      for (const perm of allPermissions) {
        granted = await SNC_requestPermission(perm.type, perm.title);
        if (!granted) { return false; }
      }
      return true;
    }
    checkAllPermissions().then(setGranted);
  }, []);

  //async componentDidMount() {
    // WifiManager.connectToProtectedSSID("WiFiMos_1", "2106198001", false).then(
    //   () => {
    //     console.log('Connected successfully!')
    //   },
    //   () => {
    //     console.log('Connection failed!')
    //   }
    // );

    // WifiManager.getCurrentWifiSSID()
    // .then((ssid) => {
    //   console.log("Your current connected wifi SSID is " + ssid)
    //   this.setState({connectedSSID: ssid});
    // }, () => {
    //   console.log('Cannot get current SSID!')
    // });
  //}
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Step One</Text>
              <Text style={styles.sectionDescription}>
                Edit <Text style={styles.highlight}>App.js</Text> to change this
                screen and then come back to see your edits.
              </Text>

              {/* кнопка входа через фейсбук */}
              <LoginButton
                publishPermissions={["email"]}
                onLoginFinished={
                  (error, result) => {
                    if (error) {
                      alert("Login failed with error: " + error.message);
                    } else if (result.isCancelled) {
                      alert("Login was cancelled");
                    } else {
                      alert("Login was successful with permissions: " + result.grantedPermissions)
                    }
                  }
                }
                onLogoutFinished={() => alert("User logged out")}/>

              {/* кнопка входа через гугл */}
              <GoogleSigninButton
                style={{ width: 192, height: 48 }}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                onPress={/*this._signIn*/ () => {alert('googleSignIn')}}
                disabled={/*this.state.isSigninInProgress*/false} />

              {/* список всех wifi-соединений */}
              {!!wifiPermissionsGranted && <SNC_WifiList/>}
            </View>
            <LearnMoreLinks />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
