<?php
$function = $_POST['function'];
if (isset($function)){
  if($function == "play-music"){
    $filename = $_POST['filename'];
    if (isset($filename)){
      $result = shell_exec("./tony-exe.sh mplayer-check");
      if($result == "true"){
        //команда pause используется как для паузы, так и для продолжения проигрывания
        echo shell_exec("./tony-exe.sh mplayer-pause \"$filename\"");
      }
      else{
        echo shell_exec("./tony-exe.sh mplayer-start \"$filename\"");
      }
    }
  }
  else if ($function == "clear-log") {
    $fileName = $_POST['filename'];
    if(isset($fileName)){
      echo "---- Clear log ----<br>";
      file_put_contents($fileName, "");
      echo nl2br( file_get_contents( $fileName ) );
    }
  }
  else if ($function == "update-log"){
    $fileName = $_POST['filename'];
    if(isset($fileName)){
      echo "---- Update log ----<br>";
      echo nl2br( file_get_contents( $fileName ) );
    }
  }
}
