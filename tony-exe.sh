#!/bin/bash
source export.sh
if [ $1 = "mplayer-check" ]; then
  # Check if gedit is running
  # -x flag only match processes whose name (or command line if -f is
  # specified) exactly match the pattern.
  if pgrep -x mplayer > /dev/null; then
    echo 'mplayer already running' >> "$TONY_LOG"
    #pgrep -x "mplayer" >> "$LOG"
    printf 'true'
  else
    printf 'false'
  fi
elif [ $1 = "mplayer-pause" ]; then
  #ставим на паузу
  echo 'pause mplayer' >> "$TONY_LOG"
  exec 3<>"$MPLAYER_OUT" #открываем канал MPLAYER_OUT для чтения и записи
  echo "pausing_keep_force run \"echo \${filename} >> $MPLAYER_OUT\"" >> "$MPLAYER_IN"
  read -r playingFile < "$MPLAYER_OUT"
  #echo "current file on server == $playingFile" >> "$TONY_LOG"
  #echo "requested file from client == $2" >> "$TONY_LOG"
  if [ "$2" = "$playingFile" ]; then
    echo "pause" >> "$MPLAYER_IN"
  else
    echo "play $2" >> "$TONY_LOG"
    echo "loadfile \"$MUSIC_FOLDER/$2\"" >> "$MPLAYER_IN"
  fi
elif [ $1 = "mplayer-start" ]; then
  #обновляем плейлист
  #echo 'create playlist' >> "$TONY"
  #find "$MUSIC_FOLDER" -type f -iname *.mp3 > "$PLAYLIST"
  #если mplayer не запущен - запускаем его в фоновом режиме(чтобы в консоль ничего не писал)
  echo 'start mplayer' >> "$TONY_LOG"
  #echo "mplayer -quiet -idle -playlist $PLAYLIST >> $MPLAYER_OUT" >> "$TONY_DAEMON"
  echo "play $2" >> "$TONY_LOG"
  echo "mplayer -slave -input file=$MPLAYER_IN -quiet -idle \"$MUSIC_FOLDER/$2\" >> $MPLAYER_LOG" >> "$TONY_IN"
  #echo ./mplayer-cmd.sh get_file_name
else
  echo "$@" >> "$TONY_LOG"
fi
