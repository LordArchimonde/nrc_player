var acc = document.getElementsByClassName("accordion");
var i;
console.log("Hello World");

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

function drawColorPicker() {
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext("2d");
  var radius = 50;
  var inRadius = 30;
  var image = ctx.createImageData(2*radius, 2*radius);
  var data = image.data;

  for (var x = -radius; x < radius; x++) {
    for (var y = -radius; y < radius; y++) {
      
      var [r, phi] = xy2polar(x, y);
      
      if (r < inRadius || r > radius) {
        // skip all (x,y) coordinates that are outside of the circle
        continue;
      }
      
      var deg = rad2deg(phi);
      
      // Figure out the starting index of this pixel in the image data array.
      var rowLength = 2*radius;
      var adjustedX = x + radius; // convert x from [-50, 50] to [0, 100] (the coordinates of the image data array)
      var adjustedY = y + radius; // convert y from [-50, 50] to [0, 100] (the coordinates of the image data array)
      var pixelWidth = 4; // each pixel requires 4 slots in the data array
      var index = (adjustedX + (adjustedY * rowLength)) * pixelWidth;
      
      var hue = deg;
      var saturation = 1.0;
      var value = 1.0;
      
      var [red, green, blue] = hsv2rgb(hue, saturation, value);
      var alpha = 255;
      
      data[index] = red;
      data[index+1] = green;
      data[index+2] = blue;
      data[index+3] = alpha;
    }
  }

  ctx.putImageData(image, 0, 0);
}

function xy2polar(x, y) {
  var r = Math.sqrt(x*x + y*y);
  var phi = Math.atan2(y, x);
  return [r, phi];
}

// rad in [-π, π] range
// return degree in [0, 360] range
function rad2deg(rad) {
  return ((rad + Math.PI) / (2 * Math.PI)) * 360;
}

// hue in range [0, 360]
// saturation, value in range [0,1]
// return [r,g,b] each in range [0,255]
// See: https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV
function hsv2rgb(hue, saturation, value) {
  var chroma = value * saturation;
  var hue1 = hue / 60;
  var x = chroma * (1- Math.abs((hue1 % 2) - 1));
  var r1, g1, b1;
  if (hue1 >= 0 && hue1 <= 1) {
    ([r1, g1, b1] = [chroma, x, 0]);
  } else if (hue1 >= 1 && hue1 <= 2) {
    ([r1, g1, b1] = [x, chroma, 0]);
  } else if (hue1 >= 2 && hue1 <= 3) {
    ([r1, g1, b1] = [0, chroma, x]);
  } else if (hue1 >= 3 && hue1 <= 4) {
    ([r1, g1, b1] = [0, x, chroma]);
  } else if (hue1 >= 4 && hue1 <= 5) {
    ([r1, g1, b1] = [x, 0, chroma]);
  } else if (hue1 >= 5 && hue1 <= 6) {
    ([r1, g1, b1] = [chroma, 0, x]);
  }

  var m = value - chroma;
  var [r,g,b] = [r1+m, g1+m, b1+m];

  // Change r,g,b values from [0,1] to [0,255]
  return [255*r,255*g,255*b];
}

drawColorPicker();