<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Material music player</title>
  <link href="https://fonts.googleapis.com/css?family=RobotoDraft:400,500,700,400italic" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="css/style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" />
  <script src="js/nrc-player.js" defer></script>
</head>

<body>
  <div class="player">
	<div class="main-cover">
    <nav class="nav">
      <div class="left">
        <i class="material-icons">menu</i>
        <h6>Playlist</h6>
      </div>
      <div class="right">
        <i class="material-icons search">search</i>
        <i class="material-icons">queue_music</i>
      </div>
    </nav>
    <div class="player-ui">
  		<div class="title">
  			<h3>Hello</h3>
  		</div>
  		<div class="small">
  			<i class="material-icons">replay</i>
  			<p>Adele</p>
  			<i class="material-icons">volume_up</i>
  		</div>
  		<div class="progress">
  			<div class="played">
  				<div class="circle"></div>
  			</div>
  		</div>
  		<div class="controls">
  			<i class="material-icons">skip_previous</i>
  			<i class="material-icons">play_arrow</i>
  			<i class="material-icons">skip_next</i>
  		</div>
  	</div>
  	<!--div class="btn">
      <i class="material-icons">shuffle</i>
  	</div-->
	</div>
  <div class="music">
    <?php foreach(new DirectoryIterator('music') as $atrist) {  ?>
      <?php if($atrist->isDir()) { ?>
        <?php foreach(new DirectoryIterator("music/$atrist") as $song) {  ?>
          <?php if($song->isFile() && ($song->getExtension() == "mp3")) { ?>
            <div class="song">
              <div class="info">
        				<div class="img" style="background-image:url('music/<?php echo $atrist . '/' . 'cover-mini.jpg'; ?>')"></div>
        				<div class="titles">
        					<h5> <?php echo $song->getBasename(".mp3"); ?> </h5>
        					<p> <?php echo $atrist; ?> </p>
        				</div>
        			</div>
              <!--div class="state playing"-->
        			<div class="state">
        				<!--i class="material-icons">equalizer</i-->
                <i class="material-icons">play_arrow</i>
              </div>
            </div>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    <?php } ?>

	</div>
</div>


</body>
</html>
