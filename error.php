<!doctype html>
<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<link rel="stylesheet" href="styles.css">
		<!--style>
			body {background-color: powderblue;}
			h1   {color: blue;}
			p    {color: red;}
		</style-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!--Обработчики нажатия кнопок отчистить и обновить лог-->
		<?php
			class NRC_Log{
				var $description;
				var $fileName;
				var $class;
				function __construct($description, $fileName, $class) {
						$this->description = $description;
		        $this->fileName = $fileName;
						$this->class = $class;
		    }
			}
			$allNrcLogs["myTerminal"] = new NRC_Log("TONY USER LOG", "tony.log", "bg-warning text-dark"); //пока неактивен
			$allNrcLogs["apacheLog"] = new NRC_Log("APACHE ERRORS", "/var/log/apache2/error.log", "bg-danger text-white");
			$allNrcLogs["mplayerLog"] = new NRC_Log("MPLAYER LOG", "mplayer.log", "bg-success text-white");
		?>
		<script>
			$(document).ready(function(){
				<?php	foreach ($allNrcLogs as $id => $data) { ?>
					// Кнопка очистки лога
					$("#button-clear-<?php echo $id; ?>").click(function(){
						$("#log-content-<?php echo $id; ?>").load("tony-exe.php", { function:"clear-log", filename:"<?php echo $data->fileName; ?>" });
					});
					// Кнопка обновления лога
					$("#button-update-<?php echo $id; ?>").click(function(){
						$("#log-content-<?php echo $id; ?>").load("tony-exe.php", { function:"update-log", filename:"<?php echo $data->fileName; ?>" });
					});
				<?php } ?>
				// Отправка комманды серверу
				$( "#sendCommandForm" ).submit(function( event ) {
				  // Stop form from submitting normally
				  event.preventDefault();
				  // Get some values from elements on the page:
				  var $form = $( this ),
				    term = $form.find( "input[name='command']" ).val(),
				    url = "tony-exe.php";
				  // Send the data using post
				  var posting = $.post( url, { command: term } );
				  // Put the results in a div
				  posting.done(function( data ) {
				    var content = $( data ).find( "#content" );
				    //$( "#commandResult" ).empty().append( content );
						$( "#commandResult" ).append( content );
				  });
					$( "#commandResult" ).append("try submit form" + "<br>url = " + url + "<br>term = " + term + "<br>");
				});
			});
		</script>
		<title>Error log</title>
	</head>
	<body>
		<!-- Вывод терминала сервера(от лица пользовалеля www-data) -->
		<!-- Форма для ввода и отправки комманд -->
		<form id="sendCommandForm">
  		<input type="text" name="command" placeholder="Enter command there...">
  		<input type="submit" value="Выполнить">
		</form>
		<div class="row bg-primary text-white" id="commandResult">
		</div>
		<div class="row">
			<?php	foreach ($allNrcLogs as $id => $data) { ?>
				<div class="col-md-4 border">
					<div class="row border"><?php echo $data->description; ?></div>
					<div class="row border">
						<button id="button-clear-<?php echo $id; ?>">Очистить</button>
						<button id="button-update-<?php echo $id; ?>">Обновить</button>
					</div>
					<div id="log-content-<?php echo $id; ?>" class="<?php echo $data->class; ?>">
						<?php echo nl2br( file_get_contents( $data->fileName ) ); ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</body>
</html>
