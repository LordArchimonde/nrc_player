#!/bin/bash
source export.sh
#echo 'Try create FIFO'
#if [ -p "$TONY_IN" ]; then :
#  echo 'Fifo already exist'
#elif mkfifo -m 777 "$TONY_IN"; then :
#  echo 'Fifo created'
#else
#  echo 'Error creating fifo'
#  exit 1
#fi
if [ -p "$TONY_IN" ]; then :; elif mkfifo -m 777 "$TONY_IN"; then :; else exit 1; fi
if [ -p "$MPLAYER_IN" ]; then :; elif mkfifo -m 777 "$MPLAYER_IN"; then :; else exit 1; fi
if [ -p "$MPLAYER_OUT" ]; then :; elif mkfifo -m 777 "$MPLAYER_OUT"; then :; else exit 1; fi
# очищаем поток входных команд, чтобы удалить команды из предыдущей сессии
dd if="$TONY_IN" iflag=nonblock of=/dev/null
dd if="$MPLAYER_IN" iflag=nonblock of=/dev/null
dd if="$MPLAYER_OUT" iflag=nonblock of=/dev/null
echo 'Daemon successfully started'
# двоеточие означает пустую комманду(возвращающую success, для бесконечного цикла)
while :; do
    while read -r cmd; do
        if [ "$cmd" ]; then
            printf 'Running %s ...\n' "$cmd"
            bash -c "$cmd" > "$TONY_OUT" 2>> "$SERVER_LOG" &
        fi
    done <"$TONY_IN"
done
