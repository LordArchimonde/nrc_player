<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Settings</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="css/style2.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" />
  <script src="js/nrc-settings.js?<?php echo date('l jS \of F Y h:i:s A'); ?>" defer></script>
</head>
<body>
    <div class="settings-list">
    <div class="back"></div>
    <div class="scrollbar-y-hidden-block">        
    <div class="content">

    <button class="accordion">Alarm(s)</button>
    <div class="panel">
        <input type="time" id="appt-time" name="appt-time" min="9:00" max="18:00" required />
        <?php 
            $jfile = file_get_contents('settings.json');
            $decoded_json = json_decode($jfile);
        ?>
        <label class="switch">
            <input type="checkbox" checked><span class="slider round"></span>
        </label><br><br>
    </div>

    <button class="accordion">Time/Date</button>
    <div class="panel add-notify">
        <!--p>Lorem ipsum...</p-->
        <!--input type="time" id="appt-time" name="appt-time" step="1" min="9:00" max="18:00" required /-->
        <div class="time-digit">
            HOURS <br>
            <input type="text" placeholder="12">
        </div>
        <div class="time-digit">
            MINUTES <br>
            <input type="text" placeholder="34">
        </div>
        <div class="time-digit">
            SECONDS <br>
            <input type="text" placeholder="56">
        </div>
    </div>

    <button class="accordion">RGB backlight</button>
    <div class="panel">
    <canvas id="canvas"></canvas>
    </div>

    <button class="accordion">Wifi/Internet</button>
    <div class="panel">
    <p>Lorem ipsum...</p>
    </div>

    <button class="accordion">Bluetooth</button>
    <div class="panel">
    <p>Lorem ipsum...</p>
    </div>

    <button class="accordion">Other</button>
    <div class="panel">
    <p>Lorem ipsum...</p>
    </div>

    </div>
    </div>
    </div>
</body>
</html>