<?php
/**
 * Online playlist creator
 *
 * @version 0.1
 * @author Anatoliy Belsky
 * @copyright copyleft
 */
 
/**
 * Path to the media files
 */
$dir = '/var/www/html/music';
 
/**
 * Address where the files are accessible through the web
 */
$url = 'http://192.168.9.2/music';
 
/**
 * Which file extensions we want to read
 */
$ext = array('mp3', 'ogg');
 
/**
 * Filelist container
 */
$files = array();
 
 
/**
 * Read the filelist and build urls
 */
foreach(new DirectoryIterator($dir) as $item) {
        if($item->isFile() && in_array(strtolower(substr($item, -3)), $ext)) {
                $files[] = "$url/$item";
        }
}
 
/**
 * Sort filelist
 */
$sort_method = isset($_GET['sort']) ? $_GET['sort'] : 'rand';
switch($sort_method) {
        /**
         * Alphabetical order
         */
        case 'alpha':
                asort($files);
        break;
 
        /**
         * Reversed alphabetical order
         */
        case 'ralpha':
                arsort($files);
        break;
 
        /**
         * Natural case insensitive order
         */
        case 'natcase':
                natcasesort($files);
        break;
 
        case 'rand':
        default:
                srand((float)microtime() * 1000000);
                shuffle($files);
        break;
}
 
/**
 * Manage the output format
 */
$format = isset($_GET['format']) ? $_GET['format'] : 'm3u';
switch($format) {
        case 'txt':
                $mime_type = 'text/plain';
        break;
 
        case 'm3u':
        default:
                $mime_type = 'audio/x-mpequrl';
        break;
}
 
/**
 * Output headers and the playlist contents
 */
header('Status: 200 OK');
header("Content-Type: $mime_type");
header('Content-Disposition: inline; filename="playlist.m3u"');
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
 
print join("\n", $files);
