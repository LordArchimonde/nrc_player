<input type="text" value="<?php echo $fileContent ?>" id="myInput">
<button onclick="document.getElementById('myInput').value = ''">Clear input field</button>

-msglevel :<модуль>=<уровень>:...>

ОСНОВНЫЕ ОПЦИИ
-quiet
Делает консольный вывод менее подробным; в частности,
прячет статусную строку т.е. A: 0.7 V: 0.6 A-V: 0.068 ...).
Может быть полезно на медленных или неполноценных терминалах, которые некорректно обрабатывают возврат каретки (т.е. \r).

-------------ОПЦИИ ПРОИГРЫВАТЕЛЯ (ТОЛЬКО MPLAYER)-------------
-idle (смотрите также -slave)
Если не осталось больше файлов для воспроизведения,
MPlayer будет ждать вместо завершения работы.
Весьма полезно при запуске в подчиненном режиме, когда MPlayer управляется через команды.

-(no)gui
Включает или выключает GUI интерфейс (значение по умолчанию зависит от имени исполняемого файла).
Работает только как первый параметр командной строки. Не работает в файле конфигурации.

-playlist <имя_файла>
Воспроизводит файлы в соответствии со списком (ASX, Winamp, SMIL, или по-одному-файлу-в-строке формата).
ЗАМЕЧАНИЕ: Опция считается элементом, так что все последующие опции будут применяться только к элементам списка воспроизведения.
FIXME: Это требует тщательного разъяснения и описания.

-slave (смотрите также -input)
Включает ведомый режим, в котором MPlayer работает в качестве бэкэнда к другим программам.
Вместо перехвата событий клавиатуры, MPlayer будет читать со стандартного входа команды, разделенные символом новой строки (\n).
ЗАМЕЧАНИЕ: Смотрите список этих команд в -input cmdlist и описания в DOCS/tech/slave.txt.
Эта опция не предназначена для отключения других источников ввода, например при помощи окна видео;
для данных целей используйте иные способы, например, -input nodefault-binds:conf=/dev/null.

-noconsolecontrols
Не допускает чтение событий клавиатуры со стандартного входа программой MPlayer.
Полезно при чтении данных со стандартного входа. Включается автоматически,
если в командной строке найдено -. Есть ситуации, когда нужно установить это вручную,
например если вы открываете /dev/stdin (или аналогичный в вашей системе),
используете stdin в списке воспроизведения или собираетесь читать из stdin позже командами loadfile или loadlist.

-----------ОПЦИИ ВЫВОДА ЗВУКА (ТОЛЬКО MPLAYER)----------------
-softvol
-softvol-max <10.0-10000.0>
-volstep <0-100>
-volume <-1-100> (смотрите также -af volume)

echo "pausing run \"echo \${filename} >> /home/tony/nrc_player/mplayer_out.txt\"" > /home/tony/.mplayer/fifo
